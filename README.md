# Rapport de TP de Recherche Opérationnelles

<u>Instructions:</u>
- Ce qui à motivé votre choix de format (lp ou mod/dat) de la modélisation
- Quelques détails sur les points clés et non triviaux de votre modélisation (ce que représentent vos variables de décisions, une contrainte élaborée...)
- Une courte argumentation de l’adéquation du résultat avec l’instance résolue (la solution obtenue fait-elle sens dans le contexte défini par l’énoncé ?)
- Quelques éléments d’analyse, par exemple :
    - Pour PL : Les matrices de ces exemples sont-elles creuses ? (Dans la pratique, il est fréquent qu’une contrainte ne rassemble que de 5 à 10 variables.)
    - Pour PLNE : En combien d’itérations est trouvée la solution optimale continue ? Combien de fois GLPK a amélioré la meilleure solution entière ? Combien d’itérations du simplexe ont été nécessaires ? Combien de nœuds de l’arbre ont été explorés ?

## Exercice 1 : Des voitures

```bash
glpsol --lp exo1/voitures.lp -o exo1/voitures.sol
```

- Nous avons choisi d'utiliser un `.lp` pour résoudre ce problème puisque celui-ci est simple et ses données ne changent pas.
- Voici comment nous avons choisi de modéliser le problème:
    - `nS` $`\in \N`$ modélise le nombre de voiture Standard produit par semaine.
    - `nL` $`\in \N`$ modélise le nombre de voiture de Luxe produit par semaine.
    - On cherche à maximiser `Benefice`.
    - `CapaciteParking` modélise la surface maximale du parking.
    - `TempsTravail` modélise le temps de travail maximal de employés.
    - `LimiteLuxe` limite le nombre de voiture de Luxe produisable.
- 3 itérations sont nécéssaires pour résoudre le problème du simplex. 4 itérations sont nécéssaires pour trouver la meilleure solution entière. 3 noeuds de l'arbre ont été explorés.
- On obtient comme solution: `nS` = 645 et `nL` = 426. Ce resultat est cohérent.

## Exercice 2 : Gestion de personnel

```bash
glpsol -m exo2/personnel.mod -d exo2/personnel.dat -o exo2/personnel.sol
```

- Nous avons choisi de modéliser ce problème en utilisant un `.mod` et un `.dat` puisque l'input de matrices de se fait plus simplement dans un `.dat`. De plus, il est dépendant de N donc il est suceptible d'évoluer doncs seul le fichier `.dat` sera à modifier car le fichier `.mod` est plus général.
- Voici comment nous avons choisi de modéliser le problème:
    - $`(`$`perm`$`)_{i,j} \in M_2(\{0,1\})`$ modélise l'association d'un travail à une personne.
    - `perm` modélise les association qui doivent être uniques ; donc c'est une matrice de permutation. (ie. chaque ligne et chaque colonne ne doit contenir un seul 1)
- 10 itérations sont nécéssaires pour résoudre le problème du simplex. 10 itérations sont nécéssaires pour trouver la meilleure solution entière. 1 noeud de l'arbre à été exploré.
- Pour les données que l'on a fournis, on obtient la solution :
$`\left(
\begin{array}{cccc} 
    1 & 0 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 0 & 1 
\end{array}
\right)`$

Cette solution est bien la meilleur solution car chaque personne est associé au travail ou il est le plus efficace en vu des paramètres que nous avions posés.

## Exercice 3 : En bourse

```bash
glpsol --lp exo3/bourse.lp -o exo3/bourse.sol
```

- Nous avons choisi d'utiliser un `.lp` pour résoudre ce problème puisque celui-ci est simple et ses données ne changent pas.
- Voici comment nous avons choisi de modéliser le problème:
    - `p1` modélise le produit financier n°1: *Crédits commerciaux*
    - `p2` modélise le produit financier n°2: *Obligations de sociétés*
    - `p3` modélise le produit financier n°3: *Stocks d'Or*
    - `p4` modélise le produit financier n°4: *Stocks de Platine*
    - `p5` modélise le produit financier n°5: *Titres hypotécaires*
    - `p6` modélise le produit financier n°6: *Prếts de construction*
    - On cherche à maximiser `Interets`.
    - `SommeP` contraint d'investir l'ensemble du budget.
    - `InvestissementProduit_i` limite l'investissement dans un même produit à 25%.
    - `Risque` limite le risque global de l'investisement à 2,0.
    - `MetauxPrecieuxMin` contraint d'investir au moins 30% dans les métaux précieux.
    - `CreditsMin` contraint d'investir au moins 45% dans les crédits commerciaux et obligations.
- On note `10 rows, 6 columns, 22 non-zeros`, la matrice de résolution est creuse et remplie à 36%.
- On obtient comme solution:
    - `p1` = 0.2
    - `p2` = 0.25
    - `p3` = 0.107692
    - `p4` = 0.192308
    - `p5` = 0.25
    - `p6` = 0

## Exercice 4 : En optimisation pour l'e-commerce

```bash
glpsol -m exo4/ecommerce.mod -d exo4/ecommerce.dat -o exo4/ecommerce.sol
```

- Nous avons choisi de modéliser ce problème en utilisant un `.mod` et un `.dat` puisque l'input de matrices de se fait plus simplement dans un `.dat`. De plus, il est dépendant de N donc il est suceptible d'évoluer doncs seul le fichier `.dat` sera à modifier car le fichier `.mod` est plus général.
- Voici comment nous avons choisi de modéliser le problème:
    - $`(`$`coef`$`)_{i,j,k} \in M_3(\R_+)`$ modélise l'association d'un trvail à une personne.
    - Chaque coefficient de `coef` doit être compris entre 0 et 1 puisque ce sont des proportions.
    - La somme des coefficients de `coef` selon l'axe k doit faire 1.
    - La répartions des commandes ne doit pas dépasser la limite des stocks.
- On note `23 rows, 12 columns, 45 non-zero`, la matrice de résolution est creuse et remplie à 16%.
- On obtient la solution : (C'est la répartition de chaque fluides de chaque commande sur chaque magasin)
$`
\left(
\begin{array}{c} 
\left(
\begin{array}{c} 
    3/4 \\
    1/4 \\
    0
\end{array}
\right) &
\left(
\begin{array}{c} 
    1 \\
    0 \\
    0
\end{array}
\right) \\
\left(
\begin{array}{c} 
    1 \\
    0 \\
    0
\end{array}
\right) &
\left(
\begin{array}{c} 
    1/3 \\
    1/3 \\
    1/3
\end{array}
\right)
\end{array}
\right)
`$

Cette solution est bien la meilleur solution car chaque personne est associé au travail ou il est le plus efficace en vu des paramètres que nous avions posés.

## Exercice bonus : Des composants

```bash
glpsol -m exobonus/bonus.mod -d exobonus/bonus.dat -o exobonus/bonus.sol
```

marche pas mais on a cherché `;(`