############################  Model ##################################


############################  Sets  ##################################

set PERSONNELS; 

set TRAVAUX;	

#########################  Variables  ################################

var perm{i in TRAVAUX, j in PERSONNELS}, binary; 

##################  Constants: Data to load  #########################

param Cout{i in TRAVAUX, j in PERSONNELS}; 

########################  Constraints  ###############################

# Chaque Travail doit être affecté a une unique Personne
s.t. RespectUnTravailParPersonne{i in TRAVAUX}:
  sum{j in PERSONNELS} perm[i,j] = 1;

# Chaque Personne doit être affectée à un unique Travail
s.t. RespectUnePersonneParTravail{j in PERSONNELS}:
  sum{i in TRAVAUX} perm[i,j] = 1;

########################  Objective  #################################

minimize CoutTotal:
	sum{i in TRAVAUX, j in PERSONNELS} Cout[i, j] * perm[i, j];

end;
