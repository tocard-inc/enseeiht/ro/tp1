Problem:    personnel
Rows:       9
Columns:    16 (16 integer, 16 binary)
Non-zeros:  48
Status:     INTEGER OPTIMAL
Objective:  CoutTotal = 4 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 RespectUnTravailParPersonne[t1]
                                   1             1             = 
     2 RespectUnTravailParPersonne[t2]
                                   1             1             = 
     3 RespectUnTravailParPersonne[t3]
                                   1             1             = 
     4 RespectUnTravailParPersonne[t4]
                                   1             1             = 
     5 RespectUnePersonneParTravail[p1]
                                   1             1             = 
     6 RespectUnePersonneParTravail[p2]
                                   1             1             = 
     7 RespectUnePersonneParTravail[p3]
                                   1             1             = 
     8 RespectUnePersonneParTravail[p4]
                                   1             1             = 
     9 CoutTotal                   4                             

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 perm[t1,p1]  *              1             0             1 
     2 perm[t1,p2]  *              0             0             1 
     3 perm[t1,p3]  *              0             0             1 
     4 perm[t1,p4]  *              0             0             1 
     5 perm[t2,p1]  *              0             0             1 
     6 perm[t2,p2]  *              0             0             1 
     7 perm[t2,p3]  *              1             0             1 
     8 perm[t2,p4]  *              0             0             1 
     9 perm[t3,p1]  *              0             0             1 
    10 perm[t3,p2]  *              1             0             1 
    11 perm[t3,p3]  *              0             0             1 
    12 perm[t3,p4]  *              0             0             1 
    13 perm[t4,p1]  *              0             0             1 
    14 perm[t4,p2]  *              0             0             1 
    15 perm[t4,p3]  *              0             0             1 
    16 perm[t4,p4]  *              1             0             1 

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
