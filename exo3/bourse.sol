Problem:    
Rows:       10
Columns:    6
Non-zeros:  22
Status:     OPTIMAL
Objective:  Interets = 0.2825384615 (MAXimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 SommeP       NS             1             1             =   -0.00923077 
     2 InvestissementProduit1
                    B            0.2                        0.25 
     3 InvestissementProduit2
                    NU          0.25                        0.25     0.0569231 
     4 InvestissementProduit3
                    B       0.107692                        0.25 
     5 InvestissementProduit4
                    B       0.192308                        0.25 
     6 InvestissementProduit5
                    NU          0.25                        0.25      0.701538 
     7 InvestissementProduit6
                    B              0                        0.25 
     8 Risque       NU             2                           2     0.0538462 
     9 MetauxPrecieuxMin
                    B            0.3           0.3               
    10 CreditsMin   NL          0.45          0.45                  -0.0123077 

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 p1           B            0.2             0               
     2 p2           B           0.25             0               
     3 p3           B       0.107692             0               
     4 p4           B       0.192308             0               
     5 p5           B           0.25             0               
     6 p6           NL             0             0                 -0.00692308 

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 2.22e-16 on row 8
        max.rel.err = 4.44e-17 on row 8
        High quality

KKT.PB: max.abs.err = 5.55e-17 on row 9
        max.rel.err = 4.27e-17 on row 9
        High quality

KKT.DE: max.abs.err = 2.78e-17 on column 3
        max.rel.err = 1.98e-17 on column 3
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
