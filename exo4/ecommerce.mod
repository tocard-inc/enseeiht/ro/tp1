############################  Model ##################################

############################  Sets  ##################################

set DEMANDES; 

set FLUIDES;

set MAGASINS;

#########################  Variables  ################################

var coef{i in DEMANDES, j in FLUIDES, k in MAGASINS}, >=0; 

##################  Constants: Data to load  #########################

param fluidesParCommandes{i in DEMANDES, j in FLUIDES}; 

param stockMagasin{k in MAGASINS, j in FLUIDES}; 

param coutUnitaires{k in MAGASINS, j in FLUIDES}; 

param coutExpedition{i in DEMANDES, k in MAGASINS};

########################  Constraints  ###############################

# Les proportions doivent être comprises entre 0 et 1
s.t. ProportionZeroUn{i in DEMANDES, j in FLUIDES, k in MAGASINS}:
  coef[i, j, k] <= 1;

# La somme des proportions doit faire 1
s.t. ProportionTotalUn{i in DEMANDES, j in FLUIDES}:
  sum{k in MAGASINS} coef[i, j, k] = 1;

# La répartions des commandes ne doit pas dépasser la limite des stocks
s.t. RespectDesStocks{j in FLUIDES, k in MAGASINS}:
  sum{i in DEMANDES} coef[i, j, k] * fluidesParCommandes[i, j] <= stockMagasin[k, j];

########################  Objective  #################################

minimize CoutTotal:
	sum{i in DEMANDES, j in FLUIDES, k in MAGASINS} (coutUnitaires[k, j] * coef[i, j, k] + coef[i, j, k] * coutExpedition[i, k]);

end;
