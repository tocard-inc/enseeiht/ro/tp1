Problem:    ecommerce
Rows:       23
Columns:    12
Non-zeros:  45
Status:     OPTIMAL
Objective:  CoutTotal = 8 (MINimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 ProportionZeroUn[d1,f1,m1]
                    B           0.75                           1 
     2 ProportionZeroUn[d1,f1,m2]
                    B           0.25                           1 
     3 ProportionZeroUn[d1,f1,m3]
                    B              0                           1 
     4 ProportionZeroUn[d1,f2,m1]
                    B              1                           1 
     5 ProportionZeroUn[d1,f2,m2]
                    B              0                           1 
     6 ProportionZeroUn[d1,f2,m3]
                    B              0                           1 
     7 ProportionZeroUn[d2,f1,m1]
                    NU             1                           1            -3 
     8 ProportionZeroUn[d2,f1,m2]
                    B              0                           1 
     9 ProportionZeroUn[d2,f1,m3]
                    B              0                           1 
    10 ProportionZeroUn[d2,f2,m1]
                    B       0.333333                           1 
    11 ProportionZeroUn[d2,f2,m2]
                    B       0.333333                           1 
    12 ProportionZeroUn[d2,f2,m3]
                    B       0.333333                           1 
    13 ProportionTotalUn[d1,f1]
                    NS             1             1             =             2 
    14 ProportionTotalUn[d1,f2]
                    NS             1             1             =             2 
    15 ProportionTotalUn[d2,f1]
                    NS             1             1             =             4 
    16 ProportionTotalUn[d2,f2]
                    NS             1             1             =             5 
    17 RespectDesStocks[f1,m1]
                    NU           2.5                         2.5         < eps
    18 RespectDesStocks[f1,m2]
                    B            0.5                           1 
    19 RespectDesStocks[f1,m3]
                    B              0                           2 
    20 RespectDesStocks[f2,m1]
                    NU             1                           1      -1.33333 
    21 RespectDesStocks[f2,m2]
                    B              1                           2 
    22 RespectDesStocks[f2,m3]
                    NU             1                           1     -0.666667 
    23 CoutTotal    B              8                             

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 coef[d1,f1,m1]
                    B           0.75             0               
     2 coef[d1,f1,m2]
                    B           0.25             0               
     3 coef[d1,f1,m3]
                    NL             0             0                           1 
     4 coef[d1,f2,m1]
                    B              1             0               
     5 coef[d1,f2,m2]
                    NL             0             0                           1 
     6 coef[d1,f2,m3]
                    NL             0             0                       < eps
     7 coef[d2,f1,m1]
                    B              1             0               
     8 coef[d2,f1,m2]
                    NL             0             0                       < eps
     9 coef[d2,f1,m3]
                    B              0             0               
    10 coef[d2,f2,m1]
                    B       0.333333             0               
    11 coef[d2,f2,m2]
                    B       0.333333             0               
    12 coef[d2,f2,m3]
                    B       0.333333             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 4.44e-16 on row 20
        max.rel.err = 1.48e-16 on row 20
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 0.00e+00 on column 0
        max.rel.err = 0.00e+00 on column 0
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
