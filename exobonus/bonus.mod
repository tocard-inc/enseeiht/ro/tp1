#      0 1 2 3 5
#      1 0 1 2 3
#      2 1 0 1 2
#      3 2 1 0 1
#      4 3 2 1 0
#      
#      0 1 0 0 0
#      0 0 1 0 0
#      1 0 0 0 0
#      0 0 0 0 1
#      0 0 0 1 0
#      
#      0 0 0 1 0
#      0 0 1 0 0
#      0 0 0 0 1
#      0 1 0 0 0
#      1 0 0 0 0
#      
#      4 2 3 5 1

###############################  Model ###############################


###############################  Sets  ###############################

set POSITIONS; 	

###################  Variables  ###################

var cycle{i in POSITIONS, j in POSITIONS}, binary; 

###################  Constants: Data to load  #########################

param Distances{i in POSITIONS, j in POSITIONS}; 

###################  Constraints  ###################

# Matrice de permutation
s.t. RespectUnTravailParPersonne{i in POSITIONS}:
  sum{j in POSITIONS} cycle[i, j] = 1;
s.t. RespectUnePersonneParTravail{j in POSITIONS}:
  sum{i in POSITIONS} cycle[i, j] = 1;

# Pas de sous cycle
s.t. SousCycle{i in POSITIONS, j in POSITIONS}:
  
  if (i <= j && i < 4) then
    sum {k in POSITIONS, l in POSITIONS} (cycle[k, l] && (i <= k <= j && i <= l <= j))  <= (j - i + 1))
  else
    true;
  

###### Objective ######

minimize CoutTotal:
	sum{i in TRAVAUX, j in PERSONNELS} Cout[i, j] * cycle[i, j];

end;
